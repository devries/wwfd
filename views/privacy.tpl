%include header privacy_active=True
    <h1>Privacy Statement</h1>
    <p>This tool is designed to let you see what information is shared with another website when you use Facebook
    social login. In order to find this information, this server obtains an access token from Facebook to access
    your data. This token is maintained on our servers for 10 minutes to allow you to perform multiple queries
    of Facebook data. All the other data obtained from Facebook is immediately discarded after the page is
    generated.</p>
    <p>This page, and the content within it sets a number of cookies. These are as follows:</p>
    <ul>
      <li>I use the <tt>bottle.session</tt> cookie to maintain a session so that when I query Facebook, I know
      who I should be querying for. The session cookie contains a random number which is linked in our database
      to your access token. Both the cookie and access token are set to expire in 10 minutes from your last query.</li>
      <li>The <tt>__utma</tt>, <tt>__utmb</tt>, <tt>__utmc</tt>, and <tt>__utmz</tt> cookies are set by
      google analytics. Their <a href="http://www.google.com/intl/en/policies/privacy/">privacy policy</a> covers
      the use of these cookies to collect information about you.</li>
      <li>The twitter like button sets approximately 18 cookies (this may vary by user). The use of these cookies
      is covered by the <a href="https://twitter.com/privacy">Twitter privacy policy</a>, and the 
      <a href="https://support.twitter.com/articles/20170514#">Twitter cookies help center page</a>.
      <li>The Facebook share button sets approximately 36 cookies (this may vary by user). The use of these cookies
      is covered under the <a href="https://www.facebook.com/about/privacy/">Facebook data use policy</a> and the
      <a href="https://www.facebook.com/help/cookies/">Facebook cookies help page</a>.
    </ul>
    <p>The <a href="https://bitbucket.org/devries/wwfd">source code for this application is available</a> under the open
    source Artistic License 2.0.</p>
%include footer
