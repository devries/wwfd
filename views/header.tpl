<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-movile-web-app-satus-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="An app to let you see what Facebook social login shares with third parties.">
<meta name="author" content="Christopher De Vries">
<meta name="twitter:card" content="summary">
<meta name="twitter:creator" content="@nissyen">
<meta property="og:url" content="http://wwfd.herokuapp.com/">
<meta property="og:title" content="What Does Facebook Share?">
<meta property="og:description" content="Social login is becoming more prevalent, but what do social login providers like Facebook share with social login sites? This application lets you explore that data.">
<meta property="og:type" content="website">
<meta property="fb:app_id" content="175748995965151">

<link rel="shortcut icon" href="/static/favicon.ico">

<title>What does facebook share?</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">

<!-- Custom styles for this template -->
<link href="/static/starter-template.css" rel="stylesheet">

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=175748995965151";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">What is Shared?</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="{{'active' if defined('intro_active') else ''}}"><a href="/">Introduction</a></li>
            <li class="{{'active' if defined('info_active') else ''}}"><a href="/basic_info">Basic Information</a></li>
            <li class="{{'active' if defined('friends_active') else ''}}"><a href="/friends">Friends</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="{{'active' if defined('privacy_active') else ''}}"><a href="/privacy">Privacy Statement</a></li>
                <li class="{{'active' if defined('tos_active') else ''}}"><a href="/tos">Terms of Service</a></li>
                <li class="{{'active' if defined('contact_active') else ''}}"><a href="/contact">Contact</a></li>
              </ul>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
