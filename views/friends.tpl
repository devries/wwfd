%include header friends_active=True
<h1>Here are your friends, I can see {{len(friends)}} of them</h1>
<ul>
% for p in friends:
  <li><a href="/friend/{{p['id']}}">{{p['name']}}</a>
%end
</ul>
<a href="{{next_link}}">{{next_name}}</a>
%include footer
