%include header tos_active=True
    <h1>Terms of Service</h1>
    <p>Apparently a terms of service agreement is the sort of thing people put on websites now. Anyway, if you
    happen to be looking at our website these are the terms that apply to you:</p>
      <ol>
        <li>We will try to protect your privacy as best we can. To read about that, see our <a href="/privacy">privacy
        statement</a>.</li>
        <li>We are only human, and as imperfect beings we make imperfect software. Although we try to avoid it,
        errors may crop up. If an error does crop up we will fix it as soon as we can.</li>
        <li>We are trying to give you a complete picture of the data Facebook shares with third-party websites,
        however we know that there may be omissions. If you are aware of any data that we are omitting please
        <a href="/contact">contact us</a> and let us know.</li>
        <li>We make no guarantees about this website's availability or accuracy. We try to keep it active and accurate,
        but if you are considering integrating this application into your nuclear reactor control software or
        possibly into an avionics or mission critical system where someone's life is on the line, then we would have
        to recommend against that. And seriously, do you really want your nuclear reactor to query Facebook?</li>
        <li>There is no term 5.
        <li>The code that runs this website is copyright, but open source. For more information you can always
        look at the <a href="https://bitbucket.org/devries/wwfd">source code</a>. You can criticise the source
        code as well, but that will make our coding monkey very sad.</li>
      </ol>
%include footer
