%include header intro_active=True
<h1>What Does Facebook Share?</h1>

<p>Social login (logging in with Facebook, Google+, Yahoo, etc...) is becoming increasingly
popular. This type of login, though convenient, has privacy implications. The websites you
choose to log in to will be given information about you. In many cases, this is very convenient.
For example, when you use social login the website you sign into can learn your name, and your
birthday which could help it make your experience more personal. You can also avoid
entering information on that website which you have already shared with Facebook.</p>

<p>Unfortunately it's very hard to see what sort of information you are sharing. This
application is meant to show you that information. It is also difficulty to figure
out how Facebook's <a href="https://www.facebook.com/settings/?tab=privacy">privacy settings</a>
can be modified to change this information. In some cases, after seeing what is shared,
you may wish to adjust your profile.</p>

<p>Social login is a great convenience that helps us remember fewer passwords, and enter
data fewer times. I use it, but I think it's important to know what is being shared between
Facebook and the sites I log in to.</p>

Log in below to see what Facebook shares about you:</p>

<a href="{{login_url}}"><img src="/static/active_404.png"></a>

%include footer
