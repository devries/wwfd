%include header contact_active=True
  <h1>Contact</h1>
  <p>You can <a href="https://bitbucket.org/devries/wwfd/issues/new">submit a bug report</a> is you notice any bugs
  or would like to request an enhancement. You can contact Christopher De Vries by
  <a href="https://twitter.com/intent/tweet?text=@nissyen%20">sending him a tweet</a>.</p>

  <p><a href="https://twitter.com/nissyen" class="twitter-follow-button" data-show-count="false" data-lang="en" data-size="large">Follow @nissyen</a></p>
%include footer
