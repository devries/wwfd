#!/usr/bin/env python
"""
This application is designed to let you view the information Facebook shared
when you use their social login. 

Copyright (c) 2013, Christopher De Vries.
License: Artistic License 2.0 (see LICENSE.txt)
"""

import bottle
import requests
import json
import os
import oauth_client
import urlparse
import redis
import bottle_session
import random
import string

app = bottle.app()

redis_url = urlparse.urlparse(os.environ.get('REDISCLOUD_URL','http://localhost:6379'))
client_id = os.environ.get('FACEBOOK_CLIENT_ID','None')
client_secret = os.environ.get('FACEBOOK_CLIENT_SECRET','None')
login_dialog_url = 'https://www.facebook.com/dialog/oauth'
token_url = 'https://graph.facebook.com/oauth/access_token'
redirect_url = 'http://wwfd.herokuapp.com/connect'

redis_connection_pool = redis.ConnectionPool(host=redis_url.hostname,port=redis_url.port,password=redis_url.password)

session_plugin = bottle_session.SessionPlugin(cookie_lifetime=600)
session_plugin.connection_pool = redis_connection_pool

app.install(session_plugin)

auth = oauth_client.Oauth2()
auth.set_credentials(client_id,client_secret,redirect_url)
auth.set_urls(login_dialog_url,token_url)

@bottle.hook('before_request')
def do_not_cache():
    bottle.response.set_header('Cache-Control','max-age=0, no-cache, no-store')

@bottle.route('/')
def index(session):
    state = ''.join(random.choice(string.ascii_uppercase+string.digits) for x in range(32))
    session['state']=state
    session['redirect']='/basic_info'

    full_login_url = auth.get_full_auth_url({'state':state})

    return bottle.template('index',login_url=full_login_url)

@bottle.route('/connect', method='GET')
def connect(session):
    returned_state = bottle.request.query.state
    code = bottle.request.query.code

    if session['state']==None:
        redo_login(session)
        return

    if returned_state != session['state']:
        print "Session mismatch error: %s != %s"%(returned_state,session['state'])
        return bottle.template('error',error_string='Session/Web Page call mismatch.')

    auth.obtain_tokens(code,method='GET')

    session.regenerate()

    session['access_token'] = auth.access_token

    bottle.redirect(session['redirect'] or '/basic_info')

@bottle.route('/basic_info')
def basic_info(session):
    auth.access_token = session['access_token']

    if auth.access_token==None:
        session['redirect']='/basic_info'
        redo_login(session)

    r = requests.get('https://graph.facebook.com/me',params={'access_token':auth.access_token})

    return bottle.template('info',content_string=parse_to_html(r.json()),next_name='What about my friends?',next_link='/friends')

@bottle.route('/friends')
def friends(session):
    auth.access_token = session['access_token']

    if auth.access_token==None:
        session['redirect']='/friends'
        redo_login(session)
        return

    r = requests.get('https://graph.facebook.com/me/friends',params={'access_token':auth.access_token})
    response_data = r.json()

    friend_list = response_data['data']

    while response_data['paging'].get('next',None)!=None:
        r = requests.get(response_data['paging']['next'])
        response_data = r.json()
        friend_list.extend(response_data['data'])

    return bottle.template('friends',friends=friend_list,next_name='Facebook privacy settings',next_link='https://www.facebook.com/settings/?tab=privacy')

@bottle.route('/friend/<user_id>')
def friend(session,user_id):
    auth.access_token = session['access_token']

    if auth.access_token==None:
        session['redirect']='/friend/%s'%user_id
        redo_login(session)
        return

    r = requests.get('https://graph.facebook.com/%s'%user_id,params={'access_token':auth.access_token})
    response_data = r.json()

    return bottle.template('info',content_string=parse_to_html(response_data),next_name='Back to friends',next_link='/friends')

@bottle.route('/privacy')
def privacy_statement():
    return bottle.template('privacy')

@bottle.route('/tos')
def terms():
    return bottle.template('tos')

@bottle.route('/contact')
def contact_info():
    return bottle.template('contact')

@bottle.route('/static/<filename:path>')
def send_static(filename):
    response = bottle.static_file(filename,root='./static')
    response.set_header('Cache-Control','public, max-age=3600')
    return response

@bottle.route('/favicon.ico')
def send_favicon():
    return bottle.static_file('favicon.ico',root='./static',mimetype="image/x-icon")

def redo_login(session):
    state = ''.join(random.choice(string.ascii_uppercase+string.digits) for x in range(32))
    session['state']=state

    full_login_url = auth.get_full_auth_url({'state':state})

    bottle.redirect(full_login_url)

def parse_to_html(parsable_structure):
    attributes = []
    if isinstance(parsable_structure,dict):
        attributes.append(u'<ul>')
        keys = parsable_structure.keys()
        for k in keys:
            attributes.append(u'<li><b>%s:</b> %s'%(k,parse_to_html(parsable_structure[k])))

        attributes.append(u'</ul>')
    elif isinstance(parsable_structure,list):
        attributes.append(u'<ol>')
        for item in parsable_structure:
            attributes.append(u'<li> %s'%parse_to_html(item))

        attributes.append(u'</ol>')

    else:
        attribute = unicode(parsable_structure)
        scheme = urlparse.urlparse(attribute).scheme

        if scheme=='http' or scheme=='https':
            attributes.append(u'<a href="%s">%s</a>'%(attribute,attribute))
        else:
            attributes.append(attribute)

    return ''.join(attributes)
        
if __name__ == "__main__":
    bottle.debug(True)
    bottle.run(app=app,host='localhost',port=9021)
